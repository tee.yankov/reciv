const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const readdir = promisify(fs.readdir)
const readFile = promisify(fs.readFile)
const { memoize } = require('lodash')

async function _getTiles () {
  const tileDirs = await readdir(path.resolve(__dirname, 'Tiles'))
  const tiles = await tileDirs.map((tileDir) => {
    const t = {}
    return readFile(path.resolve(__dirname, 'Tiles', tileDir, `${tileDir}.json`))
        .then((f) => Object.assign(t, JSON.parse(f))),
      readFile(path.resolve(__dirname, 'Tiles', tileDir, `${tileDir}.JPG`), { encoding: 'binary' })
        .then((image) => Object.assign(t, { image }))

      .then(() => t)
  })

  return tiles
}

const getTiles = memoize(_getTiles)

module.exports.getTiles = getTiles
