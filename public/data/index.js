const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const readdir = promisify(fs.readdir)
const readFile = promisify(fs.readFile)
const { memoize } = require('lodash')

async function _getTiles () {
  const tileDirs = await readdir(path.resolve(__dirname, 'Tiles'))
  const tiles = await Promise.all(tileDirs.map((tileDir) => {
    return readFile(path.resolve(__dirname, 'Tiles', tileDir, `${tileDir}.json`))
      .then((f) => ({ ...JSON.parse(f), tileDir }))
  }))

  return tiles
}

const getTiles = memoize(_getTiles)

module.exports.getTiles = getTiles
