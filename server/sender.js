const amqp = require('amqplib/callback_api')

amqp.connect('amqp://localhost', (err, conn) => {
  if (err) { throw err }

  conn.createChannel((err, ch) => {
    const q = 'pingpong'

    ch.assertQueue(q, { durable: false })
    setInterval(() => {
      console.log('[o] Sending Ping')
      ch.sendToQueue(q, new Buffer('Ping'))
    }, 1000)
  })
})

