const Joi = require('joi')
const { v4: uuid } = require('uuid')
const { schema: { Entity }, denormalize, normalize } = require('normalizr')

module.exports.unique = () => (value) => value.id
  ? value
  : Object.assign(value, { id: uuid() })

module.exports.saveable = (key, saveMethod) => (value) => Object.assign(value, {
  save: async () => {
    let serialized = null
    if (value.validate) {
      const v = value.validate()
      if (v.error) {
        throw new Error(v.error)
      }
      serialized = v.value
    } else {
      serialized = value.serialize()
    }
    await saveMethod(key, serialized)
  },
  serialize: () => value.toJSON()
})

module.exports.validatable = (schema) => (value) => {
  const validation = schema.validate(value, { stripUnknown: true })
  if (validation.error) {
    throw new Error(validation.error)
  }

  return Object.assign(value, {
    validate: () => schema.validate(value, { stripUnknown: true })
  })
}

module.exports.fetchable = (fetchMethod) => (value) => Object.assign(value, {
  fetchAll: async () => fetchMethod(),
  fetch: async () => fetchMethod().filter((k) => k.id === value.id)
})

module.exports.compose = (...fns) =>
  fns.reduce((prevFn, nextFn) => (value) => nextFn(prevFn(value)), (value) => value)

module.exports.nullOr = (otherType) => Joi.alternatives().try(Joi.any().allow(null), otherType)

module.exports.getter = async (getterFn) => getterFn()

module.exports.withProxy = (opts) => (value) => new Proxy(value, opts)

const normalSchemaMap = {}

module.exports.normalized = (key, definition = {}, options = {}) => (value) => {
  const remappedDefinitions = Object.entries(definition).map(([k, v]) => [k, normalSchemaMap[v]]).reduce((acc, [k, v]) => ({ ...acc, [k]: v }), {})
  const schema = new Entity(key, remappedDefinitions, options)
  normalSchemaMap[key] = schema
  return Object.assign(value, {
    normalize: () => normalize(value, schema),
    denormalize: () => denormalize(value, schema, remappedDefinitions)
  })
}
