const Router = require('koa-router')
const game = require('./api/game')

const router = new Router()

// Game Routes
router.use('/game', game.routes())
router.use('/game', game.allowedMethods())

// Catch-All
const allCatcher = (ctx, next) => {
  console.log('all')
  ctx.status = 404
  ctx.body = {
    hello: 'world',
    are: 'you lost?'
  }
}

router.use(allCatcher)

module.exports = router
