const amqp = require('amqplib/callback_api')

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    const q = 'pingpong'

    ch.assertQueue(q, { durable: false })

    console.log('[*] Waiting for Ping')
    ch.consume(q, (msg) => {
      console.log('[*] Received', msg.content.toString())
    })
  })
})
