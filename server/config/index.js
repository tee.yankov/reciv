const compose = require('koa-compose')
const bodyParser = require('koa-bodyparser')
const logger = require('./logger')
const errorHandler = require('./error-handler')
const { init } = require('./db')

const setupDB = async () => init()

const corsHandler = async (ctx, next) => {
  if (ctx.request.method === 'OPTIONS') {
    ctx.status = 204
  }
  ctx.set('Access-Control-Allow-Origin', 'http://localhost:3000')
  ctx.set('Access-Control-Allow-Headers', '*')
  ctx.set('Access-Control-Allow-Methods', 'GET,PUT,POST,OPTIONS,DELETE')

  return next()
}

const middleware = [
  corsHandler,
  logger,
  errorHandler,
  bodyParser()
]

module.exports.middleware = compose(middleware)
module.exports.setupDB = setupDB
