const Rebridge = require('rebridge')
const redis = require('redis')

let db = {}

async function init () {
  const client = redis.createClient()

  db = new Rebridge(client)

  await db.games.set([])
  console.log('DB Connection Initialized')
}

module.exports.getGames = async () => db.games._promise
module.exports.getTiles = async () => db.tiles._promise

module.exports.push = (key, value) => db[key].push(value)
module.exports.db = db
module.exports.init = init
