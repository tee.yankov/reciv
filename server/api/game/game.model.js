const Joi = require('joi')
const { compose, saveable, validatable, nullOr, fetchable, unique, normalized } = require('../../util')
const { getGames, push, getTiles } = require('../../config/db')
const { PlayerSchema } = require('../user/user.model')

const ResourceSchema = Joi.object({
  type: Joi.string().required(),
  count: Joi.number().required()
})

const SquareSchema = Joi.object().keys({
  type: Joi.string().required(),
  overlay: nullOr(Joi.object()),
  occupants: Joi.array().required(),
  resources: Joi.array().items(ResourceSchema).required()
})

const TileSchema = Joi.object().keys({
  id: Joi.string().guid({ version: ['uuidv4'] }).required(),
  civilization: nullOr(Joi.string()),
  recommendedCapitolCoordinates: nullOr(Joi.string()),
  squares: Joi.object().pattern(/\d+x\d+/, SquareSchema).required(),
  tileDir: Joi.string()
})

const GameSchema = Joi.object({
  id: Joi.string().guid({ version: ['uuidv4'] }).required(),
  name: Joi.string().min(3).max(64).trim().required(),
  tiles: Joi.array().items(TileSchema).required(),
  players: Joi.array().items(PlayerSchema).required()
})

const Tile = (tile = {}) => compose(
  unique(),
  validatable(TileSchema),
  saveable('tiles', push),
  fetchable(getTiles),
  normalized('tiles')
  // withProxy({
  //   get: (...args) => {
  //     Object.assign(args[0], { virtual: true })
  //     return Reflect.get(...args)
  //   }
  // })
)(tile)

const Game = (game = {}) => compose(
  unique(),
  validatable(GameSchema),
  saveable('games', push),
  fetchable(getGames),
  normalized('games', { tiles: ['tiles'] })
)(game)

module.exports.Tile = Tile
module.exports.Game = Game
