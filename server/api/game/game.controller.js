const { getTiles } = require('../../../public/data')
const { Game, Tile } = require('./game.model')
const { chain, flatten } = require('lodash')
const { v4: uuid } = require('uuid')
const { Player } = require('../user/user.model')

// Get ALL game tiles
module.exports.get = async (ctx) => {
  const games = await Game()
    .fetchAll()

  ctx.body = {
    data: games
  }
}

// Create new game
module.exports.create = async (ctx) => {
  const name = ctx.request.body.name
  if (!name) {
    ctx.throw(400, 'No game name provided')
  }

  const tiles = await getTiles()

  const nations = ['china', 'russia', 'america', 'rome']

  // Grab nation tiles
  const nationTiles = tiles
    .filter((k) => nations.indexOf(k.civilization) !== -1)
    .map((k) => Tile(k))

  const players = nationTiles
    .map((v) => Player({ civilization: v.civilization }))

  // Grab 12 random tiles
  const mapTiles = chain(tiles)
    .filter((k) => !k.civilization)
    .map((k) => Tile(k))
    .shuffle()
    .take(12)
    .value()

  const game = Game({
    id: uuid(),
    name,
    tiles: initGrid(nationTiles.concat(mapTiles)),
    players
  })

  await game.save()

  ctx.body = {
    data: game
  }
}

module.exports.makeMove = async (ctx) => {
  const { gameId } = ctx.request.body.name
  const game = await Game({ id: gameId }).fetch()
  ctx.body = {
    data: game
  }
}

/**
 * Position players at corners and distribute tiles inbetween
 */
const initGrid = (tiles) => {
  const grid = new Array(4).fill([]).map(() => new Array(4).fill(null))
  const setGrid = (x, y, v) => { grid[x][y] = v }
  const startingCivPositions = [[0, 0], [0, 3], [3, 0], [3, 3]]
  const [civs, normalTiles] = tiles.reduce((acc, tile) => {
    tile.civilization ? acc[0].push(tile) : acc[1].push(tile)
    return acc
  }, [[], []])
  civs.forEach((civ, i) => setGrid(...startingCivPositions[i], civ))
  grid.forEach((gs) => gs.forEach((g, i) => {
    if (!g) {
      gs[i] = normalTiles.pop()
    }
  }))
  return flatten(grid)
}
