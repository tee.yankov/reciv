const Router = require('koa-router')
const controller = require('./game.controller')

const router = new Router()

router.get('/', controller.get)
router.post('/make-move', controller.makeMove)
router.post('/', controller.create)

module.exports = router
