const Joi = require('joi')
const { compose, validatable, unique, normalized } = require('../../util')

const PlayerSchema = Joi.object().keys({
  id: Joi.string().guid({ version: ['uuidv4'] }).required(),
  civilization: Joi.string().valid('china', 'russia', 'america', 'rome')
})

const Player = (player = {}) => compose(
  unique(),
  validatable(PlayerSchema),
  normalized('players')
)(player)

module.exports.Player = Player
module.exports.PlayerSchema = PlayerSchema
