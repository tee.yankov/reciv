const Koa = require('koa')
const router = require('./router')
const { middleware, setupDB } = require('./config')

const PORT = process.env.PORT || 8080
const app = new Koa()

app.use(middleware)
app.use(router.routes())
app.use(router.allowedMethods())

setupDB()
  .then(() => {
    console.log(`Server started on port ${PORT}`)
    app.listen(PORT)
  })
