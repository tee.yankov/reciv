import { combineReducers, configureStore } from '@reduxjs/toolkit'
import game from './Game'

const getRootReducer = () => combineReducers({
  game: game.reducer,
})

const store = configureStore({
  reducer: getRootReducer(),
})

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./index.js', () => {
    const newRootReducer = getRootReducer()
    store.replaceReducer(newRootReducer)
  })
}

export default store
