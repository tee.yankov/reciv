import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  tiles: [],
  players: [],
  name: '',
  id: null
}

const slice = createSlice({
  name: 'game',
  initialState,
  reducers: {
    startGame: (state, action) => {
      state.tiles = action.payload.tiles
      state.name = action.payload.name
      state.id = action.payload.id
    }
  }
})

export const { startGame } = slice.actions

export const createGame = () => async (dispatch) => {
  const game = await window.fetch('http://localhost:8080/game', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      name: 'Asmodeus'
    })
  })
    .then((v) => v.json())
    .then(({ data }) => data)

  dispatch(startGame({
    tiles: game.tiles,
    name: game.name,
    id: game.id
  }))
}

export const makeMove = (x, y, tile) => async (dispatch, { getState }) => {
  await window.fetch('http://localhost:8080/game/make-move', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      gameId: getState().game.id,
      x,
      y,
      tileId: tile.id
    })
  })
}

export default slice
