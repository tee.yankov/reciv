import React from 'react'
import styled from 'styled-components'

const SquareElement = styled.div`
  position: relative;
  display: block;
  background-color: black;
  color: white;
`

function Square ({ children }) {
  return (
    <SquareElement>
      {children}
    </SquareElement>
  )
}

export default Square
