import React from 'react'
import Square from '../Square'
import styles from './Grid.module.css'
import { connect } from 'react-redux'
import { makeMove } from '../../Store/Game'

const CIV_COLORS = ['red', 'green', 'blue', 'yellow']

function Grid ({ tiles, handleInteraction }) {
  if (!tiles.length || !tiles[0]) {
    return null
  }

  return (
    <div className={styles.outerGrid}>
      <div className={styles.titleContainer}>
        <h1>Reciv v0.0.1</h1>
        <div>
          <ul>
            {tiles.filter((v) => v.civilization).map((v, i) => (<li key={i}>P{i + 1} - {v.tileDir} - <div className={styles.colorTile} style={{ backgroundColor: CIV_COLORS[i] }} /></li>))}
          </ul>
        </div>
      </div>
      <div className={styles.gridContainer}>
        {tiles.map((tile, x) => (
          <Square key={tile.id}>
            <img className={styles.gridImage} alt='game tile' src={`${process.env.PUBLIC_URL}/data/Tiles/${tile.tileDir}/${tile.tileDir}.JPG`} />
            <div className={styles.interactionGrid}>
              {new Array(16).fill(0).map((v, y) => (
                <div key={y} onClick={() => handleInteraction(x, y, tile)} />
              ))}
            </div>
          </Square>
        ))}
      </div>
      <div />
    </div>
  )
}

export default connect(
  (state) => ({
    tiles: state.game.tiles
  }),
  (dispatch) => ({
    handleInteraction: (...args) => dispatch(makeMove(...args))
  })
)(Grid)
