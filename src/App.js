import React, { useEffect } from 'react'
import Grid from './Components/Grid'
import styles from './App.module.css'
import { connect } from 'react-redux'
import { createGame } from './Store/Game'

function App ({ tiles, createGame }) {
  useEffect(() => {
    createGame()
  }, [createGame])

  return (
    <div className={styles.App}>
      <Grid />
    </div>
  )
}

export default connect(
  undefined,
  (dispatch) => ({
    createGame: () => dispatch(createGame())
  })
)(App)
